### What is this repository for? ###

This script does a one-way sync of [Endomondo](https://www.endomondo.com/) workouts to [SmashRun](http://smashrun.com/).
This script is a quick hack, but it works.
First a list of all workouts is downloaded from Endomondo. Workouts that have not allready been downloaded will be downloaded to the TODO folder.
Next, if there are any workouts in the TODO folder, they will be uploaded to SmashRun and moved to the DONE folder.


### How do I get set up? ###

*Dependencies*

You will need a machine with MacOs, Linux, or anything that can run bash scripts. Also curl and python (2 or higher) with the dateutil are used.
You can install these using:
````
sudo apt-get update
sudo apt-get install curl python python-pip -y
pip install python-dateutil
````

*Download and configuration*

You can download the script using the following commands:
````
wget https://bitbucket.org/erwin_os/endosmash/raw/master/SyncEndoAndSmash.sh  
chmod u+x SyncEndoAndSmash.sh 
````

Edit the script [SyncEndoAndSmash.sh](https://bitbucket.org/erwin_os/endosmash/src) and fillout the required variables at the top of the script. These variables define the login info of both Endomondo and SmashRun. Alternatively you can create a file called ~/.syncandsmashsecrets containing the variables. 
You can run the script manually or add it as a cron job to run daily.

