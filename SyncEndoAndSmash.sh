#!/usr/bin/env bash

## The following variables should be configured by you.
## Or set them in ~\.syncandsmashsecrets
ENDO_EMAIL=your@email.com
ENDO_PWD=YourEndomondoPassword
SMASH_EMAIL=your@email.com
SMASH_PWD=YourSmashRunPassword


####
#### Download gpx files from Endomondo
####
function download_endomondo() {
    # Login
    echo Loging into Endomondo
    curl -s -c 'data/endo_cookies' -X POST -b CSRF_TOKEN=ttigb1phf7t1mt8vsiu6jasclp -H "X-CSRF-TOKEN: ttigb1phf7t1mt8vsiu6jasclp" -H "Content-Type: application/json" -d '{"email":"'$ENDO_EMAIL'","password":"'$ENDO_PWD'","remember":true}' https://www.endomondo.com/rest/session  > data/endo_login.json
    ENDO_UID=$(cat data/endo_login.json | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')
    ENDO_FULLNAME=$(cat data/endo_login.json | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["first_name"]+" "+obj["last_name"]')

    # Get workout history ID's
    echo "Retreiving running workouts of $ENDO_FULLNAME (UserID $ENDO_UID)"
    curl -s -b 'data/endo_cookies' https://www.endomondo.com/rest/v1/users/$ENDO_UID/workouts/history?sport=0\&limit=999 > data/endo.json 

    # Download missing workout gpx files
    cat data/endo.json | python -c $'import json,sys;obj=json.load(sys.stdin)\nfor v in range(len(obj["data"])): print v;' > data/endoidx
    cat data/endoidx | while read -r line; do 
      # check if workout is live (still running)
      islive=$(cat data/endo.json | python -c $'import json,sys;obj=json.load(sys.stdin)\nprint obj["data"]['$line$']["is_live"]')
      # Get the ID at the current line
      cid=$(cat data/endo.json | python -c $'import json,sys;obj=json.load(sys.stdin)\nprint obj["data"]['$line$']["id"]')
      # Get the date formatted as filename.
      cfname=$(cat data/endo.json | python -c $'import json,sys,dateutil.parser;obj=json.load(sys.stdin)\nd=dateutil.parser.parse(obj["data"]['$line$']["start_time"])\nprint d.strftime("%Y%m%d_%H%M%S.gpx")')
      # Check if workout is not live and if file already exits in TODO or DONE folder
      if [ "$islive" = "False" ] && [ ! -f "todo/$cfname" ] && [ ! -f "done/$cfname" ]; then
        echo Downloading workout $cid as $cfname 
        curl -s -b 'data/endo_cookies' https://www.endomondo.com/rest/v1/users/$ENDO_UID/workouts/$cid/export?format=GPX > todo/$cfname
        cp data/endo.json data/endo_$cid.json
      elif [ ! "$islive" = "False" ]; then
        echo "Skipping live workout $cid ($cfname)"
      fi
    done
    echo Finished downloading gpx files.
}

####
#### Upload gpx files to SmashRun
####
function upload_smashrun() {

    # Anything todo?
    if ! ls todo/*.gpx 1> /dev/null 2>&1; then
      echo "Nothing to upload to SmashRun"
      return
    fi

    # Login to SmashRun
    echo Loging into SmashRun
    curl -s -c 'data/smashcookies' -X POST -H "Content-Type: application/json" -d '{"loginEmail":"'$SMASH_EMAIL'","loginPassword":"'$SMASH_PWD'","rememberMe":true}' https://secure.smashrun.com/services/user-jsonservice.asmx/LoginUser > data/smash_login.json
    SMASH_UNAME=$(cat data/smash_login.json | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["results"]["userName"]')
    
    # Upload the gpx files and move them to the DONE directory.
    for filename in todo/*.gpx; do
        echo Uploading $filename
        curl -s -b 'data/smashcookies' -s -F browseImportFile=@$filename http://smashrun.com/file-import.aspx > data/smash_lastupload.htm
        mv $filename done
    done
}

function main() {
  ## Check if variables should by overriden by ~\.syncandsmashsecrets
  if [ -f ~/.syncandsmashsecrets ]; then 
    source ~/.syncandsmashsecrets
  elif [ "$ENDO_EMAIL" = "your@email.com" ]; then
    echo Please configure this script by editing it.
    exit 1
  fi

  # make sure correct folders exist
  mkdir -p data
  mkdir -p todo
  mkdir -p done

  # sync
  download_endomondo
  upload_smashrun
}

main

